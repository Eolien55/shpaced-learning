Shell spaced-learning implementation. Using Anki's algorithm to determine when
to ask questions.

The script is conceived to be short and very extendable (rewriting some
messages and a dialog function should suffice to adapt it to any workflow).

`_revise` is a script that uses `revise`, dmenu and st to provide a simple
workflow (dmenu could be replaced with another X11 menu program (that'd mean
adapting the options) and st with any other terminal emulator, and setting
`REVISE_DIR` env var).
